<?php
/**
 * @OA\Info(
 *     description="RemakeScape Swagger Interface is the backbone API that manages data for all services.",
 *     version="1.0",
 *     title="RemakeScape Swagger Interface",
 *     @OA\Server(
 *     description="RemakeScape API Handle Address",
 *     url="https://api.remakescape.com/api"
 * )
 * )
 */

/**
 * @OA\Tag(
 *     name="OAuth",
 *     description="User account authorization"
 * )
 */

/**
 * @OA\Tag(
 *     name="User",
 *     description="User account handles."
 * )
 */

/**
 * @OA\Tag(
 *     name="Player",
 *     description="In game player handles."
 * )
 */

/**
 * @OA\Post(path="/oauth/token",
 *     tags={"OAuth"},
 *     summary="Generates a user auth token.",
 *     description="",
 *     operationId="oauth_token_passwordGrant",
 * @OA\Parameter(
 *     name="grant_type",
 *     required=true,
 *     in="query",
 *     description="OAuth2 standard grant types.",
 * @OA\Schema(
 *     type="string"
 * )
 * ),
 * @OA\Parameter(
 *     name="client_id",
 *     required=true,
 *     in="query",
 *     description="Client ID",
 * @OA\Schema(
 *     type="string"
 * )
 * ),
 * @OA\Parameter(
 *     name="client_secret",
 *     required=true,
 *     in="query",
 *     description="Client Secret",
 * @OA\Schema(
 *     type="string"
 * )
 * ),
 * @OA\Parameter(
 *     name="username",
 *     required=false,
 *     in="query",
 *     description="Username",
 * @OA\Schema(
 *     type="string"
 * )
 * ),
 * @OA\Parameter(
 *     name="password",
 *     required=false,
 *     in="query",
 *     description="Password",
 * @OA\Schema(
 *     type="string"
 * )
 * ),
 * @OA\Parameter(
 *     name="scope",
 *     required=true,
 *     in="query",
 *     description="The required scopes for the auth session.",
 * @OA\Schema(
 *     type="string"
 * )
 * ),
 * @OA\Response(
 *     response=200,
 *     description="Successful",
 * @OA\Schema(
 *     type="string"
 * ),
 * @OA\Header(
 *     header="X-Rate-Limit",
 * @OA\Schema(
 *     type="integer",
 *     format="int32"
 * ),
 *     description="Calls per hour allowed by the user."
 * )
 * ),
 * @OA\Response(
 *     response=400,
 *     description="Invalid"
 * )
 * )
 */