<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @OA\Schema(
 *     schema="User",
 *     type="object",
 * @OA\Property(
 *     property="id",
 *     type="integer",
 *     format="bigint20"
 * ),
 * @OA\Property(
 *     property="username",
 *     type="string"
 * ),
 * @OA\Property(
 *     property="email",
 *     type="string"
 * ),
 * @OA\Property(
 *     property="password",
 *     type="string"
 * )
 * )
 */

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function findForPassport($identifier) {
        return $this->where('email',$identifier)->orWhere('username', $identifier)->first();
    }
}
