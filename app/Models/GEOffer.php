<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GEOffer extends Model
{
    protected $table = "ge_offers";
}
