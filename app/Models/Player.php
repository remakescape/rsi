<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     schema="Player",
 *     type="object",
 * @OA\Property(
 *     property="id",
 *     type="integer",
 *     format="bigint20"
 * ),
 * @OA\Property(
 *     property="user_id",
 *     type="integer",
 *     format="int256"
 * ),
 * @OA\Property(
 *     property="display_name",
 *     type="string"
 * ),
 * @OA\Property(
 *     property="previous_xteas",
 *     type="json"
 * ),
 * @OA\Property(
 *     property="x",
 *     type="integer"
 * ),
 * @OA\Property(
 *     property="z",
 *     type="integer"
 * ),
 * @OA\Property(
 *     property="height",
 *     type="integer"
 * ),
 * @OA\Property(
 *     property="privilege",
 *     type="integer"
 * ),
 * @OA\Property(
 *     property="display_mode",
 *     type="integer"
 * ),
 * @OA\Property(
 *     property="run_energy",
 *     type="double"
 * ),
 * @OA\Property(
 *     property="appearance",
 *     type="json"
 * ),
 * @OA\Property(
 *     property="skills",
 *     type="json"
 * ),
 * @OA\Property(
 *     property="attributes",
 *     type="json"
 * ),
 * @OA\Property(
 *     property="timers",
 *     type="json"
 * ),
 * @OA\Property(
 *     property="item_containers",
 *     type="json"
 * ),
 * @OA\Property(
 *     property="varps",
 *     type="json"
 * )
 * )
 */

class Player extends Model
{
    protected $table = 'players';

    protected $fillable = [
        'user_id',
        'display_name',
        'appearance',
        'skills',
        'attributes',
        'timers',
        'item_containers',
        'varps'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function ge_offers() {
        return $this->hasMany("App\Models\GEOffer", 'player_id', 'id');
    }
}
