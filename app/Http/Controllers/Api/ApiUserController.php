<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;

class ApiUserController extends Controller {

    public function __construct() {
    }

    /**
     * @OA\Get(path="/user/{username}",
     *     summary="Get user by username.",
     *     description="Returns user information in json.",
     *     operationId="getUser",
     *     tags={"User"},
     * @OA\Parameter(
     *     name="username",
     *     required=true,
     *     in="path",
     *     description="In-game name or username to return.",
     * @OA\Schema(
     *     type="string"
     * )
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Successful operation",
     * @OA\JsonContent(ref="#/components/schemas/User")
     * ),
     * @OA\Response(
     *     response=404,
     *     description="Username not found."
     * ),
     *     security={
     *     {"rsi_oauth": {"read.user"}}
     *     }
     * )
     */

    public function getUser($username) {
        $user = User::where('username', $username)->first();

        if($user === null) {
            return response('user_not_found', 404);
        }

        return response()->json($user);
    }

}