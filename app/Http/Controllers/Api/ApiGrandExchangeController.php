<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\GEOffer;
use App\Models\Player;
use App\User;
use Illuminate\Http\Request;

class ApiGrandExchangeController extends Controller {
    public function createOffer(Request $request)
    {
        $user = auth()->user();
        $slot = $request->slot;
        $state = $request->state;
        $item_id = $request->item_id;
        $price = $request->price;
        $quantity = $request->quantity;
        $type = $request->type;

        $player = Player::where('user_id',$user->id)->first();

        if(GEOffer::where(['player_id' => $player->id, 'slot' => $slot])->count() > 0) {
            return response("slot_not_available", 400);
        }

        $offer = new GEOffer;
        $offer->player_id = $player->id;
        $offer->slot = $slot;
        $offer->state = $state;
        $offer->item_id = $item_id;
        $offer->price = $price;
        $offer->quantity = $quantity;
        $offer->quantityFilled = 0;
        $offer->spent = 0;
        $offer->type = $type;
        $offer->save();
        return response("offer_created_success", 200);
    }

    public function getOffers(Request $request) {
        $user = $request->user();
        if($user == null) { return response("", 401); }

        $player = Player::where('user_id',$user->id)->first();
        $offers = GEOffer::where('player_id',$player->id)->get();
        $json = array();

        foreach($offers as $offer) {
            $json[count($json)] = array(
                "id" => $offer->id,
                "username" => $user->username,
                "slot" => $offer->slot,
                "state" => $offer->state,
                "item_id" => $offer->item_id,
                "price" => $offer->price,
                "quantity" => $offer->quantity,
                "quantityFilled" => $offer->quantityFilled,
                "spent" => $offer->spent,
                "type" => $offer->type
            );
        }

        return response(json_encode($json), 200);
    }

    public function updateOffer(Request $request, $slot) {
        $user = $request->user();
        $player = Player::where('user_id',$user->id)->first();

        $offer = GEOffer::where(['player_id' => $player->id, 'slot' => $slot])->first();
        if($offer == null) {
            return response("offer_not_found", 404);
        }

        $state = $offer->state+$request->statedelta;

        $offer->state = $state;
        $offer->save();
        return response("offer_updated_success", 200);
    }

    public function getUpdatedOffers() {
        $offers = GEOffer::where('updated', true)->get();
        $json = array();

        foreach($offers as $offer) {
            $player = Player::where('id',$offer->player_id)->first();
            $user = User::where('id',$player->user_id)->first();
            $json[count($json)] = array(
                "id" => $offer->id,
                "username" => $user->username,
                "slot" => $offer->slot,
                "state" => $offer->state,
                "item_id" => $offer->item_id,
                "price" => $offer->price,
                "quantity" => $offer->quantity,
                "quantityFilled" => $offer->quantityFilled,
                "spent" => $offer->spent,
                "type" => $offer->type
            );
            $offer->updated = false;
            $offer->save();
        }
        return response(json_encode($json), 200);
    }

    public function getPlayerOffer(Request $request, $slot) {
        $user = $request->user();
        $player = Player::where('user_id',$user->id)->first();
        $offer = GEOffer::where(['player_id' => $player->id, 'slot' => $slot])->first();

        if($offer == null) {
            return response("offer_not_found", 404);
        }

        $json = array(
            "id" => $offer->id,
            "username" => $user->username,
            "slot" => $offer->slot,
            "state" => $offer->state,
            "item_id" => $offer->item_id,
            "price" => $offer->price,
            "quantity" => $offer->quantity,
            "quantityFilled" => $offer->quantityFilled,
            "spent" => $offer->spent,
            "type" => $offer->type
        );
        return response(json_encode($json), 200);
    }

    public function deleteOffer(Request $request, $slot) {
        $user = $request->user();
        $player = Player::where('user_id',$user->id)->first();
        $offer = GEOffer::where(['player_id' => $player->id, 'slot' => $slot])->first();

        if($offer == null) { return response("offer_not_found", 404); }

        $offer->delete();
        return response("delete_offer_success", 200);
    }
}