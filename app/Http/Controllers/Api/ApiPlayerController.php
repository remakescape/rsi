<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Player;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiPlayerController extends Controller {

    public function __construct() {}


    public function getPlayer($username) {
        $user = User::where('username', urldecode($username))->first();

        if($user === null) {
            return response('user_not_found', 404);
        }

        $player = Player::where('user_id',$user->id)->first();

        if($player === null) {
            return response('player_not_found', 404);
        }

        $data = '
        {
            "passwordHash": "",
            "username": "'. $user->username . '",
            "displayName": "'. $player->display_name .'",
            "previousXteas": '.$player->previous_xteas.',
            "x": '.$player->x.',
            "z": '.$player->z.',
            "height": '.$player->height.',
            "privilege": '.$player->privilege.',
            "displayMode": '.$player->display_mode.',
            "runEnergy": '.$player->run_energy.',
            "appearance": '.$player->appearance.',
            "skills": '.$player->skills.',
            "attributes": '.$player->attributes.',
            "timers": '.$player->timers.',
            "itemContainers": '.$player->item_containers.',
            "varps": '.$player->varps.'
        }';

        return response($data, 200);
    }

    public function updatePlayer(Request $request) {
        $json = json_decode($request->data);
        $userid = Auth::id();

        $player = Player::where('user_id',$userid)->first();

        if($player === null) {
            $p = new Player;
            $p->user_id = $userid;
        } else {
            $p = $player;
        }

        $p->display_name = $json->displayName;
        $p->previous_xteas = json_encode($json->previousXteas);
        $p->x = $json->x;
        $p->z = $json->z;
        $p->height = $json->height;
        $p->privilege = $json->privilege;
        $p->display_mode = $json->displayMode;
        $p->run_energy = $json->runEnergy;
        $p->appearance = json_encode($json->appearance);
        $p->skills = json_encode($json->skills);
        $p->attributes = json_encode($json->attributes);
        $p->timers = json_encode($json->timers);
        $p->item_containers = json_encode($json->itemContainers);
        $p->varps = json_encode($json->varps);

        $p->save();

        return response('success', 200);
    }

}