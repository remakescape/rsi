FROM ubuntu:bionic

WORKDIR /root

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install software-properties-common -y
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt-get update

RUN apt-get install -y php7.2 php7.2-common php7.2-pdo-mysql php7.2-mysql php7.2-fpm php7.2-mbstring php7.2-xml php7.2-json php7.2-zip php7.2-curl
RUN apt-get install -y nginx

COPY . .

RUN rm -rf /var/www/html/*
RUN rm -rf /etc/nginx/sites-available/*
RUN rm -rf /etc/nginx/sites-enabled/*

RUN cp docker/nginx/rsi /etc/nginx/sites-available/
RUN ln -s /etc/nginx/sites-available/rsi /etc/nginx/sites-enabled/rsi

RUN cp -R . /var/www/html/
RUN chown -R www-data:www-data /var/www/html/
RUN chmod -R 777 /var/www/html/storage
RUN chmod -R 777 /var/www/html/bootstrap

WORKDIR /var/www/html/

RUN cp .env.example .env

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"

RUN php composer.phar install --optimize-autoloader --no-dev

RUN chmod +x docker/start.sh

CMD docker/start.sh
