# RemakeScape Swagger Interface
This is the backbone for the RemakeScape OAuth2 server and swagger api. It handles the data for the game servers
as well as any other services.

### Requirements
* PHP >= 7.1.3
* PHP Composer
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* MySQL PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### Installation for Development
1. `composer install --optimize-autoloader`<br>
This command installs all of the need project dependencies through composer.

2. `cp .env.example .env`<br>
Update the settings within the .env config file before continuing.

3. `php artisan config:cache`<br>
Create a cache of the config to increase speed of the application. If this is a local development, skip this step.

4. `php artisan route:cache`<br>
Create a cache of the current routes for quicker application response. Skip this step again for local development deployments.

5. `php artisan migrate`<br>
WARNING. This updates the database structure and should not be done in production without knowing what you are doing.

6. `php artisan key:generate`<br>
Generate an encryption key that gets stored in the .env file.
