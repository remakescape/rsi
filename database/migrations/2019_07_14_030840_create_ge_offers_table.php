<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ge_offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("player_id");
            $table->integer("slot");
            $table->integer("state");
            $table->integer("item_id");
            $table->integer("price");
            $table->integer("quantity");
            $table->integer("quantityFilled");
            $table->integer("spent");
            $table->integer("type");
            $table->boolean("updated")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ge_offers');
    }
}
