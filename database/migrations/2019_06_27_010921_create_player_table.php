<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('display_name')->nullable();
            $table->longText('previous_xteas')->nullable();
            $table->integer('x')->default(3094);
            $table->integer('z')->default(3107);
            $table->integer('height')->default(0);
            $table->integer('privilege')->default(0);
            $table->integer('display_mode')->default(1);
            $table->double('run_energy')->default(100.0);
            $table->longText('appearance')->nullable();
            $table->longText('skills')->nullable();
            $table->longText('attributes')->nullable();
            $table->longText('timers')->nullable();
            $table->longText('item_containers')->nullable();
            $table->longText("varps")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player');
    }
}
