<?php

Route::get('login', function() {
   return view('app.auth.auth', ['page' => 'login']);
})->name('login');

Route::post('login', ['uses' => 'Auth\LoginController@login']);

Route::get('register', function() {
    return view('app.auth.auth', ['page' => 'register']);
})->name('register');

Route::post('register', ['uses' => 'Auth\RegisterController@register']);