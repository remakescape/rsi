<?php

// /api/user/{username}
Route::get('/user/{username}', ['uses' => 'Api\ApiUserController@getUser'])->middleware('auth:api');

// /api/player/{username}
Route::get('/player/{username}', ['uses' => 'Api\ApiPlayerController@getPlayer'])->middleware('auth:api');

// /api/player
Route::put('/player', ['uses' => 'Api\ApiPlayerController@updatePlayer'])->middleware('auth:api');

// /api/grandexchange/offer
Route::post('/grandexchange/offer', ['uses' => 'Api\ApiGrandExchangeController@createOffer'])->middleware('auth:api');

// /api/grandexchange/offer
Route::get('/grandexchange/offer', ['uses' => 'Api\ApiGrandExchangeController@getOffers'])->middleware('auth:api');

// /api/grandexchange/offer/$slot
Route::put('/grandexchange/offer/slot/{slot}', ['uses' => 'Api\ApiGrandExchangeController@updateOffer'])->middleware('auth:api');

// /api/grandexchange/offer/updated
Route::get('/grandexchange/offer/updated', ['uses' => 'Api\ApiGrandExchangeController@getUpdatedOffers'])->middleware('auth:api');

// /api/grandexchange/offer/slot/$slot
Route::get('/grandexchange/offer/slot/{slot}', ['uses' => 'Api\ApiGrandExchangeController@getPlayerOffer'])->middleware('auth:api');

// /api/grandexchange/offer/slot/$slot
Route::delete('/grandexchange/offer/slot/{slot}', ['uses' => 'Api\ApiGrandExchangeController@deleteOffer'])->middleware('auth:api');