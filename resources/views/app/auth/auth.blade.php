<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="_nK">
    <title>RemakeScape | Login or Register</title>

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300i,400,400i,700%7cMarcellus+SC" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/assets/vendor/bootstrap/dist/css/bootstrap.min.css">

    <!-- FontAwesome -->
    <script defer src="/assets/vendor/fontawesome-free/js/all.js"></script>
    <script defer src="/assets/vendor/fontawesome-free/js/v4-shims.js"></script>

    <!-- IonIcons -->
    <link rel="stylesheet" href="/assets/vendor/ionicons/css/ionicons.min.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/assets/vendor/revolution/css/settings.css">
    <link rel="stylesheet" href="/assets/vendor/revolution/css/layers.css">
    <link rel="stylesheet" href="/assets/vendor/revolution/css/navigation.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="/assets/vendor/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" href="/assets/vendor/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" href="/assets/vendor/photoswipe/dist/default-skin/default-skin.css">

    <!-- DateTimePicker -->
    <link rel="stylesheet" href="/assets/vendor/jquery-datetimepicker/build/jquery.datetimepicker.min.css">

    <!-- Summernote -->
    <link rel="stylesheet" href="/assets/vendor/summernote/dist/summernote-bs4.css">

    <!-- GODLIKE -->
    <link rel="stylesheet" href="/assets/css/godlike.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="/assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>


</head>


<!--
    Additional Classes:
        .nk-page-boxed
-->
<body>
<!-- START: Page Preloader -->
<div class="nk-preloader">
    <!--
         Preloader animation
         data-close-... data used for closing preloader
         data-open-...  data used for opening preloader
    -->
    <div class="nk-preloader-bg"
         style="background-color: #000;"
         data-close-frames="23"
         data-close-speed="1.2"
         data-close-sprites="/assets/images/preloader-bg.png"
         data-open-frames="23"
         data-open-speed="1.2"
         data-open-sprites="/assets/images/preloader-bg-bw.png">
    </div>

    <div class="nk-preloader-content">
        <div>
            <h1 class="nk-title h1 text-center">RemakeScape</h1>
            <div class="nk-preloader-animation"></div>
        </div>
    </div>

    <div class="nk-preloader-skip">Skip</div>
</div>
<!-- END: Page Preloader -->


<!--
START: Page Video Background

Additional Attributes:
data-video - Youtube/Vimeo/self-hosted video urls.
    self-hosted video example:
    data-video="mp4:./video/local-video.mp4,webm:./video/local-video.webm,ogv:./video/local-video.ogv"
data-video-loop - loop video (true/false)
data-video-mute - mute video music (true/false)
data-video-volume - volume of video music (0-100)
data-video-start-time - video start time in seconds
data-video-end-time - video end time in seconds
data-video-pause-on-page-leave - pause video when the page not in focus (true/false)
-->
<!-- END: Page Background -->

<!--
    START: Page Audio Background

    Additional Attributes:
        data-audio - URL to audio file
        data-audio-volume - audio volume (0-100)
        data-audio-autoplay - autoplay audio (true/false)
        data-audio-loop - loop audio (true/false)
        data-audio-pause-on-page-leave - pause video when the page not in focus (true/false)
-->
<!-- END: Page Background -->



<!-- START: Page Border -->
<div class="nk-page-border">
    <div class="nk-page-border-t"></div>
    <div class="nk-page-border-r"></div>
    <div class="nk-page-border-b"></div>
    <div class="nk-page-border-l"></div>
</div>
<!-- END: Page Border -->







<!--
    Additional Classes:
        .nk-header-opaque
-->
<header class="nk-header nk-header-opaque">

    <!--
        START: Navbar

        Additional Classes:
            .nk-navbar-sticky
            .nk-navbar-transparent
            .nk-navbar-autohide
            .nk-navbar-light
            .nk-navbar-no-link-effect
    -->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide">
        <div class="container">
            <div class="nk-nav-table">

                <a href="/" class="nk-nav-logo">
                    <h3 class="nk-title h3 text-center mb-0">RemakeScape</h3>
                </a>

            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>

<!--
START: Navbar Mobile

Additional Classes:
.nk-navbar-left-side
.nk-navbar-right-side
.nk-navbar-lg
.nk-navbar-overlay-content
.nk-navbar-light
.nk-navbar-no-link-effect
-->
<div id="nk-nav-mobile" class="nk-navbar nk-navbar-side nk-navbar-left-side nk-navbar-overlay-content d-lg-none">
    <div class="nano">
        <div class="nano-content">
            <a href="index.html" class="nk-nav-logo">
                <img src="assets/images/logo.svg" alt="" width="90">
            </a>
            <div class="nk-navbar-mobile-content">
                <ul class="nk-nav">
                    <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END: Navbar Mobile -->



<div class="nk-main">


    <!-- START: Header Title -->
    <!--
        Additional Classes:
            .nk-header-title-sm
            .nk-header-title-md
            .nk-header-title-lg
            .nk-header-title-xl
            .nk-header-title-full
            .nk-header-title-parallax
            .nk-header-title-parallax-opacity
            .nk-header-title-boxed
    -->
    <div class="nk-header-title nk-header-title-sm nk-header-title-parallax nk-header-title-parallax-opacity">
        <div class="bg-image">
            <img src="/assets/images/image-1.jpg" alt="" class="jarallax-img">
        </div>
    </div>
    <!-- END: Header Title -->

<!--
START: Sign Form

Additional Classes:
.nk-sign-form-light
-->
<div class="nk-sign-form open" style="padding-top:146px; display:block; opacity:1; transform:translate3d(0px,0px,0px);">
    <div class="nk-gap-5"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3">
                <div class="nk-sign-form-container">
                    <div class="nk-sign-form-toggle h3">
                        <a href="/login" class="{{($page == 'login')?" active":""}}">Log In</a>
                        <a href="/register" class="{{($page == 'register'?" active":"")}}">Register</a>
                    </div>
                    <div class="nk-gap-2"></div>

                    <!-- START: Login Form -->
                    <form class="nk-sign-form-login{{($page == 'login')?" active":""}}" action="/login" method="post">

                        @if(session('registered'))
                            <div class="nk-info-box bg-success">
                                <div class="nk-info-box-icon">
                                    <i class="fa-icon-check-circle"></i>
                                </div>
                                Your have successfully registered and account. Please login to continue.
                            </div>
                        @endif

                        @if($errors->any())
                            <div class="nk-info-box bg-danger">
                                <div class="nk-info-box-icon">
                                    <i class="ion-alert-circled"></i>
                                </div>
                                Invalid Email Address or Password.
                            </div>
                        @endif

                        @csrf
                        <input class="form-control" type="text" placeholder="Username or Email" name="email" value="{{old('email')}}">
                        <div class="nk-gap-2"></div>

                        <input class="form-control" type="password" placeholder="Password" name="password">
                        <div class="nk-gap-2"></div>

                        <div class="form-check float-left">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="remember">
                                Remember Me
                            </label>
                        </div>
                        <button type="submit" class="nk-btn nk-btn-color-white link-effect-4 float-right">Log In</button>

                        <div class="clearfix"></div>
                        <div class="nk-gap-1"></div>
                        <a class="nk-sign-form-lost-toggle float-right" href="#">Lost Password?</a>
                    </form>
                    <!-- END: Login Form -->

                    <!-- START: Lost Password Form -->
                    <form class="nk-sign-form-lost" action="#">
                        <input class="form-control" type="text" placeholder="Username or Email">
                        <div class="nk-gap-2"></div>

                        <button class="nk-btn nk-btn-color-white link-effect-4 float-right">Get New Password</button>
                    </form>
                    <!-- END: Lost Password Form -->

                    <!-- START: Register Form -->
                    <form class="nk-sign-form-register{{($page == 'register')?" active":""}}" action="/register" method="post">

                        @if($errors->any())

                            <div class="nk-info-box bg-danger">
                                <div class="nk-info-box-icon">
                                    <i class="ion-alert-circled"></i>
                                </div>
                                Oh snap! Please review the following errors.<br>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @csrf
                        <input class="form-control" type="text" placeholder="Username" name="username" value="{{old('username')}}">
                        <div class="nk-gap-2"></div>

                        <input class="form-control" type="email" placeholder="Email" name="email" value="{{old('email')}}">
                        <div class="nk-gap-2"></div>

                        <input class="form-control" type="password" placeholder="Password" name="password" value="{{old('password')}}">
                        <div class="nk-gap-2"></div>

                        <input class="form-control" type="password" placeholder="Confirm Password" name="password_confirmation" value="{{old('password_confirmation')}}">
                        <div class="nk-gap-2"></div>

                        <button class="nk-btn nk-btn-color-white link-effect-4 nk-btn-block" type="submit">Register</button>
                    </form>
                    <!-- END: Register Form -->
                </div>
            </div>
        </div>
    </div>
    <div class="nk-gap-5"></div>
</div>
<!-- END: Sign Form -->


</div>


<!-- START: Scripts -->

<!-- Object Fit Polyfill -->
<script src="/assets/vendor/object-fit-images/dist/ofi.min.js"></script>

<!-- GSAP -->
<script src="/assets/vendor/gsap/src/minified/TweenMax.min.js"></script>
<script src="/assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<!-- Popper -->
<script src="/assets/vendor/popper.js/dist/umd/popper.min.js"></script>

<!-- Bootstrap -->
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Sticky Kit -->
<script src="/assets/vendor/sticky-kit/dist/sticky-kit.min.js"></script>

<!-- Jarallax -->
<script src="/assets/vendor/jarallax/dist/jarallax.min.js"></script>
<script src="/assets/vendor/jarallax/dist/jarallax-video.min.js"></script>

<!-- imagesLoaded -->
<script src="/assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>

<!-- Flickity -->
<script src="/assets/vendor/flickity/dist/flickity.pkgd.min.js"></script>

<!-- Isotope -->
<script src="/assets/vendor/isotope-layout/dist/isotope.pkgd.min.js"></script>

<!-- Photoswipe -->
<script src="/assets/vendor/photoswipe/dist/photoswipe.min.js"></script>
<script src="/assets/vendor/photoswipe/dist/photoswipe-ui-default.min.js"></script>

<!-- Typed.js -->
<script src="/assets/vendor/typed.js/lib/typed.min.js"></script>

<!-- Jquery Validation -->
<script src="/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- Jquery Countdown + Moment -->
<script src="/assets/vendor/jquery-countdown/dist/jquery.countdown.min.js"></script>
<script src="/assets/vendor/moment/min/moment.min.js"></script>
<script src="/assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>

<!-- Hammer.js -->
<script src="/assets/vendor/hammerjs/hammer.min.js"></script>

<!-- NanoSroller -->
<script src="/assets/vendor/nanoscroller/bin/javascripts/jquery.nanoscroller.js"></script>

<!-- SoundManager2 -->
<script src="/assets/vendor/soundmanager2/script/soundmanager2-nodebug-jsmin.js"></script>

<!-- DateTimePicker -->
<script src="/assets/vendor/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js"></script>

<!-- Revolution Slider -->
<script src="/assets/vendor/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/assets/vendor/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/assets/vendor/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="/assets/vendor/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/assets/vendor/revolution/js/extensions/revolution.extension.navigation.min.js"></script>

<!-- Keymaster -->
<script src="/assets/vendor/keymaster/keymaster.js"></script>

<!-- Summernote -->
<script src="/assets/vendor/summernote/dist/summernote-bs4.min.js"></script>

<!-- GODLIKE -->
<script src="/assets/js/godlike.min.js"></script>
<script src="/assets/js/godlike-init.js"></script>
<!-- END: Scripts -->

</body>
</html>